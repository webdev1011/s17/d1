/*mobilephone - properties(key) > color, weight, model (value)
			- methods(functions) > open, alarm, close, ring, send


user (object) > properties > username, email, password, gender
			  > methods > login, logout

let grades - [85, 95, 100]

Structure of an Object

let phone = {
		color : 'red',
		weight: '10 g',
		model : 'S7'

}

let grades = {
		quiz1 : 86,
		quiz2 : 95,
		quiz3 : 100
}

//Object can contain another obeject

let user = {
	firstname : 'Juan',
	lastname : 'Dela Cruz',
	address : {
				houseNum : 11,
				street : 'Sapphire',
				city : 'Tokyo',
				country : 'Japan'
	}
}
//can contains array
let user = {
	firstname : 'Juan',
	lastname : 'Dela Cruz',
	address : {
				houseNum : 11,
				street : 'Sapphire',
				city : 'Tokyo',
				country : 'Japan'
	},
	email : ['juandelacruz@yahoo.com', 'jc@gmail.com'],
	fullName : functions() {
		return this.firstname + '' + this.lastname;
	}
}
console.log(user);*/

//let phone = Object();
//let mobilephone = {};

//adding properties
//let phone = Object();
		//phone.color = 'blue',
		//phone.weight = '115 g',
		//phone.model = 'S7',
		//phone.ring = function(){
		//	console.log('phone is ringing')
		//}

//console.log(phone)

//let mobilephone = {
	//color : 'red',
	//weight : '125 g',
	//model : 'iphone10',
	//ring : function(){
	//	console.log("mobile phone is ringing")
	//}


//};
//console.log(mobilephone);
//accessing object properties = dot notation
//console.log(phone.model);

//accessing object preperties = square brackets
//console.log(mobilephone['model']);

//reassign values
//mobilephone.color = 'black';
//mobilephone.ring = function() {
	//console.log("black is ringing")
//}

//console.log(mobilephone.color);
//console.log(mobilephone.ring);

//delete

//delete mobilephone.color
//console.log(mobilephone);

//delete mobilephone.ring
//console.log(mobilephone);

// simple constructor

//let Pokemon = {
	//	name : 'pikachu',
	//	attack : 'lightning',
	//	health : 100,

//}
//console.log(Pokemon);

//object contructor is a function that initialize object
/*
function Pokemon(name){
	this.name = "pokemon name"
	this.attack = "pokemon attack"

}*/

/*function Pokemon(name) {
	this.name = name
	this.health = 100
	this.attack = function(target){
		console.log(`${this.name} attacked ${target.name}`);
		target.health-= 10;
		console.log(`${target.name}'s health is now ${target.health}`);
		target.health += 20;
		console.log(`${this.name}'s health is now ${target.health}`);
	}
}
let Pikachu = new Pokemon("Pikachu");
let Geodude = new Pokemon("Geodude");
Pikachu.attack(Geodude); */


//function declaration 
// name = function name
// param = placeholder

// statemnt = body

function generateFullName(firstName ,middleInitial, lastName){
	return firstName + '' + middleInitial + ',' + lastName;
}

// in es6 arrow function exp syntax

() => {
	statement
}

const variable = () => {
 	statement
}
// ex of arrow function expression
const generateFullName = (firstName,middleInitial,lastName) =>{
	return firstName + ' ' + middleInitial + '. ' + lastName;
}
/* arrow function curly bracket can be ommited but still can return val*/
// syntax
const add = (x,y) => x + y

// longer
const add = (x,y) => {
	return x + y;
}
// note when do we used arrow function
// can be used when you donot need to name a function or it has a simole task to do
 // we will used the normal function
 let numbers = [1, 2, 3, 4, 5]

 let numbersMap = numbers.map(function(number){
 	return number * number
 });
 console.log(numbersMap)
 //in arrowfunction
 
let numbersMap = numbers.map( number => number * number);
console.log(numbersMap)

// template literals
// are sting literals allowing embeded exp(concatination)
const generateFullName = (firstName,middleInitial,lastName) =>{
	return firstName + ' ' + middleInitial + '. ' + lastName;
}

//syntax:
`string text`
`string text line 1`
`string text line 2`

`string text ${expression} string text`
//template literals can contain placeholders. we use dollar sign and curly brackets. the expression in the placeholder and the text between the backticks gets past to a function

//concatination
`${expression}, ${expression}`
console.log(`string text line 1
	string text line 2`)

const generateFullName = (firstName,middleInitial,lastName) =>{
	return `${firstName}  ${middleInitial}. ${lastName}`
}

// can also do comp or eval inside
`${ 8 * 6 }`

const add = (x, y) => {
	return x + y
}

//the sum is 
const add = (x, y) => `The sum is ${x+y}`;

// destructuring arrays and objects
// destructuring ass syntax is a js expression that makes it possible to unpack volumes form arrays or properties or objects into distinct variables.

// normal array
let numbers = [10, 20]
let num = `${num[0]} ${num[1]}`

//destructuring
let a, b, rest;
[a, b] = [10, 20]
console.log(a) 
console.log(b)

[a, b, ...rest] = [10, 20, 30, 40, 50]

// exp an array that contains the name of a person if we want to combine it to full name we will need to do it like this

let name = ["Juan","Dela", "Cruz"];
let fullName = `${name[0]} ${name[1]} ${name[2]}`

// destructured
let firstName,middleName,lastName ;
[firstName,middleName,lastName] = ["Juan", "Dela", "Cruz"];

let fullName =`${firstName} ${middleName} ${lastName}`
console.log(fullName)
 // destructuring
 //normal object
const person = {
	firstName: "Juan",
	middleName: "Dela",
	lastName: "Cruz"
}
const generateFullName = (firstname, middleName, lastname) =>{
return ` ${firstname} ${middleName} ${lastname}`
}
generateFullName(person.firstname)

//destructured object
const person = {
	firstName: "Juan",
	middleName: "Dela",
	lastName: "Cruz"
}
const generateFullName = ({firstname, middleName, lastname}) =>{
return ` ${firstname} ${middleName} ${lastname}`
}
generateFullName(person)